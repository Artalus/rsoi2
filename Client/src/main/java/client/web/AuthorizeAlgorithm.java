package client.web;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Random;

@Service
public class AuthorizeAlgorithm {

    private String clientId = "ivanoviv";
    private String accessToken;
    private String refreshToken;
    private String csrfCode = generateHeximalString(16);

    private static final String urlServer_authorize = "http://localhost:8081/authorize";
    private static final String urlServer_access = "http://localhost:8081/authorize/accessToken";
    private static final String urlServer_refresh = "http://localhost:8081/authorize/refreshToken";
    private static final String url_callback = "http://localhost:8082/authorize/authorizationCode";

    public String getAuthorizeUrl() {
        return String.format("%s?clientId=%s&response_type=code&callbackUrl=%s&csrf=%s",
                urlServer_authorize, clientId, url_callback, csrfCode);
    }

    /**
     * Request and save access token from server
     * @param authorizationCode code gotten from server
     * @return success
     */
    public boolean requestAccessToken(String authorizationCode) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization",
                String.format("authorizationCode=%s&response_type=bearer&csrf=%s",
                        authorizationCode, csrfCode));

        return requestTokenFromServer(urlServer_access, headers);
    }

    /**
     * Refresh acquired access token
     * @return success
     */
    public boolean requestTokenRefresh() {
        assert !refreshToken.isEmpty();


        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization",
                String.format("refreshToken=%s&response_type=refresh_token&csrf=%s",
                        refreshToken, csrfCode));

        return requestTokenFromServer(urlServer_refresh, headers);
    }

    /**
     * Request acquiring of refreshing of token from server
     * @param urlString url of action
     * @param headers headers corresponding to access
     * @return success
     */
    private boolean requestTokenFromServer(String urlString, HttpHeaders headers) {
        URI uri = UriComponentsBuilder.fromUriString(urlString).build().toUri();
        HttpEntity httpEntity = new HttpEntity<>(null, headers);

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<OAuth2Token> response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, OAuth2Token.class);
        OAuth2Token oAuth2Token = response.getBody();

        if (oAuth2Token.getCsrf().equals(csrfCode)) {
            accessToken = oAuth2Token.getToken();
            refreshToken = oAuth2Token.getRefreshToken();
            return true;
        }
        return false;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getCsrf() {
        return csrfCode;
    }

    private static String generateHeximalString(int hexLength) {
        Random randomService = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hexLength; i++) {
            sb.append(Integer.toHexString(randomService.nextInt()));
        }
        sb.setLength(hexLength);
        return sb.toString();
    }

}


class OAuth2Token {

    private String token;
    private String tokenType;
    private String refreshToken;
    private Long expiresIn;
    private String csrf;

    public OAuth2Token() {
        this("","","", 0L, "");
    }

    public OAuth2Token(String token, String tokenType, String refreshToken, Long expiresIn, String csrf) {
        this.token = token;
        this.tokenType = tokenType;
        this.refreshToken = refreshToken;
        this.expiresIn = expiresIn;
        this.csrf = csrf;
    }

    public String getToken() {
        return token;
    }

    public OAuth2Token setToken(String token) {
        this.token = token;
        return this;
    }

    public String getTokenType() {
        return tokenType;
    }

    public OAuth2Token setTokenType(String tokenType) {
        this.tokenType = tokenType;
        return this;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public OAuth2Token setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
        return this;
    }

    public Long getExpiresIn() {
        return expiresIn;
    }

    public OAuth2Token setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
        return this;
    }

    public String getCsrf() {
        return csrf;
    }

    public OAuth2Token setCsrf(String csrf) {
        this.csrf = csrf;
        return this;
    }
}