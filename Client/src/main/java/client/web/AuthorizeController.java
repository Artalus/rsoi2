package client.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@RestController
@RequestMapping("/authorize")
public class AuthorizeController {

    @Autowired
    private AuthorizeAlgorithm authorizeAlgorithm;

    @RequestMapping(method = RequestMethod.GET)
    public void getAuthorizationCode(HttpServletResponse httpServletResponse) {
        String address = authorizeAlgorithm.getAuthorizeUrl();
        httpServletResponse.setHeader("Location", address);
        httpServletResponse.setStatus(302);
    }

    @RequestMapping(value="/authorizationCode", method = RequestMethod.GET)
    public String getAccessToken(HttpServletRequest httpServletRequest) {
        Map<String, String[]> map = httpServletRequest.getParameterMap();
        String code = map.get("code")[0];
        String csrf = map.get("csrf")[0];

        String csrfTek = authorizeAlgorithm.getCsrf();
        if (csrf.equals(csrfTek)) {
            authorizeAlgorithm.requestAccessToken(code);
            return  "Authorization passed!";
        }
        else
            return "Authorization failed!";
    }


    @RequestMapping(value="/refreshToken", method = RequestMethod.GET)
    public String getRefreshToken() {
        if (authorizeAlgorithm.requestTokenRefresh()) {
            return  "Refresh ok!";
        }
        return "Refresh failed!";
    }
}
