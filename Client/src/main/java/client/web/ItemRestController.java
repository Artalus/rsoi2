package client.web;

import client.web.Requests.ItemRequest;
import client.web.Requests.ItemUpdateRequest;
import client.web.Requests.PageParams;
import client.web.Response.ItemResponse;
import client.web.Response.ListOfItemResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/item")
public class ItemRestController {
    private static final String url_item = "http://localhost:8081/item";

    @Autowired
    private AuthorizeAlgorithm authorizeAlgorithm;

    @RequestMapping(value = "/addItem", method = RequestMethod.GET)
    public ModelAndView addItemPage() {
        return new ModelAndView("addItemPage");
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/addItem", method = RequestMethod.POST)
    public String createItem(ItemRequest itemRequest) {

        HttpEntity<ItemRequest> requestUpdate = new HttpEntity<>(itemRequest, createHeadersFromToken(authorizeAlgorithm.getAccessToken()));

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Long> response = restTemplate.exchange(url_item, HttpMethod.POST, requestUpdate, Long.class);

        if (response.getStatusCodeValue() == 401) {
            return "Refresh token required";
        }

        Long itemId = response.getBody();
        return "Item added: id=" + itemId.toString();
    }

    @RequestMapping(value = "/getItem", method = RequestMethod.GET)
    public ModelAndView getItemPage() {
        return new ModelAndView("getItemPage");
    }

    @RequestMapping(value = "/getItem", method = RequestMethod.POST)
    public ItemResponse getItem(Integer id) {
        String address = String.format("%s/%d", url_item, id);
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();

        RestTemplate restTemplate = new RestTemplate();
        return new ItemResponse(restTemplate.getForObject(uri, ItemResponse.class));
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<ItemResponse> getAll() {

        URI uri = UriComponentsBuilder.fromUriString(url_item).build().toUri();

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<ListOfItemResponses> response = restTemplate.getForEntity(uri, ListOfItemResponses.class);

        if (response.hasBody())
            return response.getBody().getItemResponses();
        return null;
    }

    @RequestMapping(value = "/getPage", method = RequestMethod.GET)
    public ModelAndView getAllPagination() {
        return new ModelAndView("getItemPaginationPage");
    }

    @RequestMapping(value = "/getPage", method = RequestMethod.POST)
    public List<ItemResponse> getAllPagination(PageParams pageParams) {
        String address = String.format("%s/%d/%d", url_item, pageParams.getPageSize(), pageParams.getPageIndex());
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<ListOfItemResponses> response = restTemplate.getForEntity(uri, ListOfItemResponses.class);

        if (response.hasBody())
            return response.getBody().getItemResponses();
        return null;
    }

    @RequestMapping(value = "/updateItem", method = RequestMethod.GET)
    public ModelAndView updateItem() {
        return new ModelAndView("updateItemPage");
    }

    @RequestMapping(value = "/updateItem", method = RequestMethod.POST)
    public String updateItem(ItemUpdateRequest itemUpdateRequest) {
        String address = String.format("%s/%d", url_item, itemUpdateRequest.getItemId());
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();

        ItemRequest itemRequest = new ItemRequest(itemUpdateRequest.getName(), itemUpdateRequest.getPrice());
        HttpEntity<ItemRequest> requestUpdate = new HttpEntity<>(itemRequest, createHeadersFromToken(authorizeAlgorithm.getAccessToken()));
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Long> response = restTemplate.exchange(uri, HttpMethod.PUT, requestUpdate, Long.class);

        if (response.getStatusCodeValue() == 401) {
            return "Refresh token required";
        }
        return String.format("Item updated: id=%d", response.getBody());
    }

    @RequestMapping(value = "/deleteItem", method = RequestMethod.GET)
    public ModelAndView deleteItem() {
        return new ModelAndView("deleteItemPage");
    }

    @RequestMapping(value = "/deleteItem", method = RequestMethod.POST)
    public String deleteItem(Integer id) {
        String address = String.format("%s/%d", url_item, id);
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();

        HttpEntity<ItemRequest> requestDelete = new HttpEntity<>(null, createHeadersFromToken(authorizeAlgorithm.getAccessToken()));
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Long> response = restTemplate.exchange(uri, HttpMethod.DELETE, requestDelete, Long.class);

        if (response.getStatusCodeValue() == 401) {
            return "Refresh token required";
        }
        return "Item deleted!";
    }

    private static HttpHeaders createHeadersFromToken(String accessToken)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        return headers;
    }
}
