package client.web.Requests;

import com.google.common.base.MoreObjects;

public class ItemRequest {
    private String name;
    private Integer price;

    public ItemRequest() {
        this("", 0);
    }

    public ItemRequest(ItemRequest itemRequest) {
        this(itemRequest.getName(), itemRequest.getPrice());
    }

    public ItemRequest(String name, Integer price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public ItemRequest setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getPrice() {
        return price;
    }

    public ItemRequest setPrice(Integer price) {
        this.price = price;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("price", price)
                .toString();
    }
}
