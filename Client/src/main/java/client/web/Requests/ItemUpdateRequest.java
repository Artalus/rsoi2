package client.web.Requests;

import com.google.common.base.MoreObjects;

public class ItemUpdateRequest extends ItemRequest {
    private Long itemId;

    public ItemUpdateRequest() {
        super();
        this.itemId = 0L;
    }

    public ItemUpdateRequest(ItemRequest itemRequest, Long itemId) {
        super(itemRequest);
        this.itemId = itemId;
    }

    public ItemUpdateRequest(String name, Integer price, Long itemId) {
        super(name, price);
        this.itemId = itemId;
    }

    public Long getItemId() {
        return itemId;
    }

    public ItemUpdateRequest setItemId(Long itemId) {
        this.itemId = itemId;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", this.getName())
                .add("price", this.getPrice())
                .add("itemId", itemId)
                .toString();
    }
}
