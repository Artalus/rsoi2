package client.web.Requests;

public class PageParams {
    private int pageIndex;
    private int pageSize;

    public PageParams() {
        this.pageIndex = 0;
        this.pageSize = 0;
    }

    public PageParams(int pageIndex, int pageSize) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public PageParams setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
        return this;
    }

    public int getPageSize() {
        return pageSize;
    }

    public PageParams setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }
}
