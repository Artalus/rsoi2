package client.web.Response;

import com.google.common.base.MoreObjects;

public class ItemResponse {
    private String name;
    private Integer price;

    public ItemResponse() {
        this.name = "";
        this.price = 0;
    }


    public ItemResponse(String name, Integer price) {
        this.name = name;
        this.price = price;
    }

    public ItemResponse(ItemResponse item) {
        this.name = item.getName();
        this.price = item.getPrice();
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }




    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("price", price)
                .toString();
    }
}
