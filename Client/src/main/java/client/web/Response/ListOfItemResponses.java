package client.web.Response;

import java.util.List;

public class ListOfItemResponses {

    private List<ItemResponse> itemResponses;

    public ListOfItemResponses() {
        this.itemResponses = null;
    }

    public ListOfItemResponses(List<ItemResponse> itemResponses) {
        this.itemResponses = itemResponses;
    }

    public List<ItemResponse> getItemResponses() {
        return itemResponses;
    }

    public ListOfItemResponses setItemResponses(List<ItemResponse> itemResponses) {
        this.itemResponses = itemResponses;
        return this;
    }
}
