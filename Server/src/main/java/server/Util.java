package server;

import java.util.Random;

public class Util {
    static public String generateHeximalString(int hexLength) {
        Random randomService = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hexLength; i++) {
            sb.append(Integer.toHexString(randomService.nextInt()));
        }
        sb.setLength(hexLength);
        return sb.toString();
    }
}
