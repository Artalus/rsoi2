package server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.domain.OAuth2Token;
import server.domain.User;
import server.repository.UserRepository;

import static server.Util.generateHeximalString;

@Service
public class AuthorizationService {
    private AuthorizationData authorizationData = new AuthorizationData();

    @Autowired
    private UserRepository userRepository;

    public void saveRequestData(String clientId, String csrf, String responseType, String backUrl) {
        authorizationData = new AuthorizationData(clientId, csrf, responseType, backUrl);
    }

    public AuthorizationData getAuthorizationData() {
        return authorizationData;
    }

    public String getAuthorizationCodeUrl() {
        authorizationData.generateAuthorizationCode();
        return String.format("%s?grant_type=authorization_code&%s=%s&csrf=%s",
                authorizationData.getBackUrl(),
                authorizationData.getResponseType(),
                authorizationData.getAuthorizationCode(),
                authorizationData.getCsrf());
    }

    public OAuth2Token createAccessToken(String responseType, String csrf) {
        User user = userRepository.findByClientId(authorizationData.getClientId()).get(0);
        return updateUserToken(responseType, csrf, user);
    }

    public OAuth2Token createRefreshToken(String refreshToken, String responseType, String csrf) {
        User user = userRepository.findByRefreshToken(refreshToken).get(0);
        return updateUserToken(responseType, csrf, user);
    }

    private OAuth2Token updateUserToken(String responseType, String csrf, User user) {
        user.setAccessToken(generateHeximalString(16));
        user.setRefreshToken(generateHeximalString(16));
        user.setExpiresIn(System.currentTimeMillis() + 120000L); // 2 минуты
        userRepository.save(user);
        return new OAuth2Token()
                .setToken(user.getAccessToken())
                .setTokenType(responseType)
                .setRefreshToken(user.getRefreshToken())
                .setExpiresIn(user.getExpiresIn())
                .setCsrf(csrf);
    }

}

class AuthorizationData {

    private String clientId;
    private String csrf;
    private String responseType;
    private String authorizationCode;
    private String backUrl;

    AuthorizationData() {
        this.clientId = "";
        this.csrf = "";
        this.responseType = "";
        this.authorizationCode = "";
        this.backUrl = "";
    }

    AuthorizationData(String clientId, String csrf, String responseType, String backUrl) {
        this.clientId = clientId;
        this.csrf = csrf;
        this.responseType = responseType;
        this.authorizationCode = "";
        this.backUrl = backUrl;
    }

    AuthorizationData(String clientId, String csrf, String responseType, String authorizationCode, String backUrl) {
        this.clientId = clientId;
        this.csrf = csrf;
        this.responseType = responseType;
        this.authorizationCode = authorizationCode;
        this.backUrl = backUrl;
    }

    String getClientId() {
        return clientId;
    }

    String getCsrf() {
        return csrf;
    }

    String getResponseType() {
        return responseType;
    }

    String getAuthorizationCode() {
        return authorizationCode;
    }

    String generateAuthorizationCode() {
        this.authorizationCode = generateHeximalString(16);
        return authorizationCode;
    }

    String getBackUrl() {
        return backUrl;
    }
}
