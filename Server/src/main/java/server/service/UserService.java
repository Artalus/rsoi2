package server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import server.domain.Order;
import server.domain.User;
import server.repository.UserRepository;
import server.web.Requests.UserRequest;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.springframework.data.jpa.domain.AbstractPersistable_.id;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private  OrderService orderService;

    @Transactional(readOnly = true)
    public User getById(Long id) {
        User user = userRepository.findOne(id);
        if (user == null) {
            throw new EntityNotFoundException("User '{" + id + "}' not found");
        }
        return user;
    }

    @Transactional(readOnly = true)
    public User findByClientId(String clientId) {
        List<User> users = userRepository.findByClientId(clientId);
        if (users.size() == 0) {
            return null;
        }
        return users.get(0);
    }

    public boolean checkUpToTimeToken(String token) {
        List<User> users = userRepository.findByAccessToken(token);
        if (users.size() == 0) {
            return false;
        }

        if (users.get(0).getExpiresIn() > System.currentTimeMillis())
            return true;

        return false;
    }

    @Transactional(readOnly = true)
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Transactional
    public User save(UserRequest userRequest) {
        User user = new User(userRequest.getLastName(),userRequest.getFirstName(),userRequest.getMiddleName(),
        userRequest.getClientId(),userRequest.getPasswordHash());
        return userRepository.save(user);
    }

    @Transactional
    public User update(Long id, UserRequest userRequest) {
        User user = userRepository.findOne(id);
        if (user == null) {
            throw new EntityNotFoundException("User '{" + id + "}' not found");
        }

        user.setLastName(userRequest.getLastName());
        user.setFirstName(userRequest.getFirstName());
        user.setMiddleName(userRequest.getMiddleName());
        user.setClientId(userRequest.getClientId() != null ? userRequest.getClientId() : user.getClientId());
        user.setPasswordHash(userRequest.getPasswordHash() != null ? userRequest.getPasswordHash() : user.getPasswordHash());

        return userRepository.save(user);
    }

    @Transactional
    public void delete(Long id) {
        userRepository.delete(id);
    }

    public Long addOrderToUser(Long userId, Long orderId) {
        User user = userRepository.findOne(userId);
        if (user == null) {
            throw new EntityNotFoundException("User '{" + id + "}' not found");
        }

        Order order = orderService.getById(orderId);
        user.addOrder(order);
        userRepository.save(user);
        return user.getId();
    }

    public Long delOrderFromUser(Long userId, Long orderId) {
        User user = userRepository.findOne(userId);
        if (user == null) {
            throw new EntityNotFoundException("User '{" + id + "}' not found");
        }

        Order order = orderService.getById(orderId);
        user.delOrder(order);
        userRepository.save(user);
        return user.getId();
    }

}
